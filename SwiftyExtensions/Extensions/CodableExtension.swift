//
//  CodableExtension.swift
//  MEG-Eguides-Wrapper
//
//  Created by Timothy Barnard on 26/02/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }

    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

public extension KeyedDecodingContainer {
    func decodeWrapper<T: Decodable>(key: K) -> T? {
        do {
            return try decodeIfPresent(T.self, forKey: key) ?? nil
        } catch {
            print(error)
            return nil
        }
    }

    func decodeWrapperArray<T: Decodable>(key: K) -> T? {
        do {
            return try decodeIfPresent(T.self, forKey: key)
        } catch {
            print(error)
            return nil
        }
    }
}

public extension Encodable {
    
    static func encodeWrapper<T: Codable>(_ data: Foundation.Data, format: String = "YYYY-MM-DD") -> T? {
        do {
            let decoder = JSONDecoder()
            let dateFormater = DateFormatter()
            dateFormater.calendar = Calendar.current
            dateFormater.dateFormat = format
            decoder.dateDecodingStrategy = .formatted(dateFormater)
            let object = try decoder.decode(T.self, from: data)
            return object
        } catch {
            print(error)
        }
        return nil
    }

    static func encodeWrapperArray<T: Codable>(_ data: Foundation.Data, format: String = "YYYY-MM-DD") -> [T]? {
        do {
            let decoder = JSONDecoder()
            let dateFormater = DateFormatter()
            dateFormater.calendar = Calendar.current
            dateFormater.dateFormat = format
            decoder.dateDecodingStrategy = .formatted(dateFormater)
            let object = try decoder.decode([T].self, from: data)
            return object
        } catch {
            print(error)
        }
        return nil
    }

    func jsonEncode(_ format: String) -> String? {
        do {
            let encoder = JSONEncoder()
            let dateFormater = DateFormatter()
            dateFormater.calendar = Calendar.current
            dateFormater.dateFormat = format
            encoder.dateEncodingStrategy = .formatted(dateFormater)
            let encodedData = try encoder.encode(self)
            return String(data: encodedData, encoding: .utf8)
        } catch {
            print(error)
            return nil
        }
    }

}

// Inspired by https://gist.github.com/mbuchetics/c9bc6c22033014aa0c550d3b4324411a

public struct JSONCodingKeys: CodingKey {
    public var stringValue: String
    public var intValue: Int?
    
    public init?(stringValue: String) {
        self.stringValue = stringValue
    }

    public init?(intValue: Int) {
        self.init(stringValue: "\(intValue)")
        self.intValue = intValue
    }
}

public extension KeyedDecodingContainer {

    func decode(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> Dictionary<String, Any> {
        let container = try self.nestedContainer(keyedBy: JSONCodingKeys.self, forKey: key)
        return try container.decode(type)
    }

    func decodeIfPresent(_ type: Dictionary<String, Any>.Type, forKey key: K) throws -> Dictionary<String, Any>? {
        guard contains(key) else {
            return nil
        }
        guard try decodeNil(forKey: key) == false else {
            return nil
        }
        return try decode(type, forKey: key)
    }

    func decode(_ type: Array<Any>.Type, forKey key: K) throws -> Array<Any> {
        var container = try self.nestedUnkeyedContainer(forKey: key)
        return try container.decode(type)
    }

    func decodeIfPresent(_ type: Array<Any>.Type, forKey key: K) throws -> Array<Any>? {
        guard contains(key) else {
            return nil
        }
        guard try decodeNil(forKey: key) == false else {
            return nil
        }
        return try decode(type, forKey: key)
    }

    func decode(_ type: Dictionary<String, Any>.Type) throws -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()

        for key in allKeys {
            if let boolValue = try? decode(Bool.self, forKey: key) {
                dictionary[key.stringValue] = boolValue
            } else if let stringValue = try? decode(String.self, forKey: key) {
                dictionary[key.stringValue] = stringValue
            } else if let intValue = try? decode(Int.self, forKey: key) {
                dictionary[key.stringValue] = intValue
            } else if let doubleValue = try? decode(Double.self, forKey: key) {
                dictionary[key.stringValue] = doubleValue
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedDictionary
            } else if let nestedArray = try? decode(Array<Any>.self, forKey: key) {
                dictionary[key.stringValue] = nestedArray
            }
        }
        return dictionary
    }
}

public extension UnkeyedDecodingContainer {

    mutating func decode(_ type: Array<Any>.Type) throws -> Array<Any> {
        var array: [Any] = []
        while isAtEnd == false {
            // See if the current value in the JSON array is `null` first and prevent infite recursion with nested arrays.
            if try decodeNil() {
                continue
            } else if let value = try? decode(Bool.self) {
                array.append(value)
            } else if let value = try? decode(Double.self) {
                array.append(value)
            } else if let value = try? decode(String.self) {
                array.append(value)
            } else if let nestedDictionary = try? decode(Dictionary<String, Any>.self) {
                array.append(nestedDictionary)
            } else if let nestedArray = try? decode(Array<Any>.self) {
                array.append(nestedArray)
            }
        }
        return array
    }

    mutating func decode(_ type: Dictionary<String, Any>.Type) throws -> Dictionary<String, Any> {

        let nestedContainer = try self.nestedContainer(keyedBy: JSONCodingKeys.self)
        return try nestedContainer.decode(type)
    }
}

public extension NSCoding {
    static func registerClassName() {
        let className = String(describing: type(of: self))
        NSKeyedArchiver.setClassName(className, for: self)
        NSKeyedUnarchiver.setClass(type(of: self) as? AnyClass, forClassName: className)
    }
}
