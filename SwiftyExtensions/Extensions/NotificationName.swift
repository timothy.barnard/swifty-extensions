//
//  NotificationName.swift
//  DIT Timetable v3
//
//  Created by Timothy on 19/10/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//
import Foundation

extension Notification.Name {
    static let ThemeChange =  Notification.Name.init("theme_change")
}
