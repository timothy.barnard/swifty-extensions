//
//  DoubleExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 10/02/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

public extension CGFloat {
    var doubleValue: Double {return Double(self)}
    var degressToRadians: CGFloat { return CGFloat(doubleValue * Double.pi / 180)}
    var radiansToDegress: CGFloat {return CGFloat(doubleValue * 180 / Double.pi )}
}
