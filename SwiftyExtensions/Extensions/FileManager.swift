//
//  FileManager.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import Foundation

public extension FileManager {
    
    /// Gets directory path of directory with name passed in, creates if not exists
    ///
    /// - Parameter name: directory name to create or find
    static func getDirectory(with name: String) -> URL? {
        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dataPath = documentsDirectory.appendingPathComponent(name, isDirectory: true)
            do {
                try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
                return dataPath
            } catch let error as NSError {
                print("Error creating directory: \(error.localizedDescription)")
            }
        }
        return nil
    }
    
    static var documentsDirectory: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    }
    
    /// Returns document size
    static var documentsSize: Int64 {
        guard let docURL = FileManager.documentsDirectory else {
            return 0
        }
        var size : Int64 = 0
        do {
            let files = try FileManager.default.subpathsOfDirectory(atPath: docURL.path)
            for i in 0 ..< files.count {
                size += FileManager.getFileSize(docURL.path.appending("/"+files[i]))
            }
        } catch {
            print(error)
        }
        return size
    }
    
    /// Check if file exists with name
    ///
    /// - Parameter name: the name of the file including extension
    /// - Returns: nil or string of the file path
    static func checkFileExists(_ name: String) -> URL? {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: dirPath + "/" + name) {
                return URL(fileURLWithPath: dirPath + "/" + name)
            }
        }
        return nil
    }
    
    /// Retreving image path either in documents or project bundle
    ///
    /// - Parameter name: name of file
    /// - Returns: path if exists
    static func getImagePath(_ name: String) -> String? {
        let path = FileManager.checkFileExists(name)
        if let localPath = path {
            return localPath.absoluteString
        } else {
            if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "png") {
                return bundleImagePath
            } else if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "JPG") {
                return bundleImagePath
            } else if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "jpg") {
                return bundleImagePath
            } else if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "PNG") {
                return bundleImagePath
            } else if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "JPEG") {
                return bundleImagePath
            } else if let bundleImagePath = Bundle.main.path(forResource: name, ofType: "jpeg") {
                return bundleImagePath
            }
            
        }
        return nil
    }
    
    /// Clear apps directory
    ///
    /// - Parameter directory: directory to remove all files, defaults to documents
    static func clearAppsDirectory(_ directory: SearchPathDirectory = SearchPathDirectory.documentDirectory) {
        for path in NSSearchPathForDirectoriesInDomains(directory, .userDomainMask, true) {
            guard let items = try? FileManager.default.contentsOfDirectory(atPath: path) else { return }
            for item in items {
                let completePath = path.appending("/").appending(item)
                try? FileManager.default.removeItem(atPath: completePath)
            }
        }
    }
    
    /// Reading plist value from project bundle
    ///
    /// - Parameters:
    ///   - plistName: name of the plist file
    ///   - key: the value you want to return
    /// - Returns: AnyObject if the value exists
    static func readPlist(_ plistName: String, key: String) -> AnyObject? {
        if let path = Bundle.main.path(forResource: plistName, ofType: "plist"), let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject] {
            return dict[key]
        }
        return nil
    }
    
    /// Writing file to documents
    ///
    /// - Parameters:
    ///   - name: file name including extension
    ///   - contents: file contents in which to store
    /// - Returns: Boolean value wether file saved
    @discardableResult
    static func writeFile(_ name: String, contents: String) -> Bool {
        guard let folder = FileManager.documentsDirectory else {
            return false
        }
        let fileURL = folder.appendingPathComponent(name)
        do {
            try contents.write(to: fileURL, atomically: false, encoding: .utf8)
            return true
        } catch {
            print(error)
            return false
        }
    }

    /// Reading file from documents
    ///
    /// - Parameter name: file name including extension
    /// - Returns: String contents of the file or nil
    @discardableResult
    static func readFile(_ name: String) -> String? {
        guard let folder = FileManager.documentsDirectory else {
            return nil
        }
        let fileURL = folder.appendingPathComponent(name)
        do {
            return try String(contentsOf: fileURL)
        } catch {
            print(error)
            return nil
        }
    }

    /// Reading file from documents
    ///
    /// - Parameter name: file name including extension
    /// - Returns: Data contents of the file or nil
    @discardableResult
    static func readFile(_ name: String) -> Data? {
        guard let folder = FileManager.documentsDirectory else {
            return nil
        }
        let fileURL = folder.appendingPathComponent(name)
        do {
            return try Data(contentsOf: fileURL)
        } catch {
            print(error)
            return nil
        }
    }
    
    /// Deletes file from dcouments
    ///
    /// - Parameter fileName: file name including extension
    /// - Returns: Boolean value wether file was deleted
    static func deleteFile(_ fileName: String ) -> Bool {
        guard let fileURL = FileManager.checkFileExists(fileName) else {
            return false
        }
        do {
            try FileManager.default.removeItem(atPath: fileURL.path)
            return true
        } catch {
            print("Could not clear file \(fileName): \(error)")
            return false
        }
    }
    
    /// Get file size at given path
    ///
    /// - Parameter path: path
    /// - Returns: Size of file
    static func getFileSize(_ path: String) -> Int64 {
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: path)
            let fileSizeNumber = fileAttributes[FileAttributeKey.size] as? NSNumber
            let fileSize = fileSizeNumber?.int64Value
            return fileSize ?? 0
        } catch let error {
            print(error)
            return 0
        }
    }
    
    func format(size: Int64) -> String {
        let folderSizeStr = ByteCountFormatter.string(fromByteCount: size, countStyle: ByteCountFormatter.CountStyle.file)
        return folderSizeStr
    }

}

public extension String {
    
    func fileURL(_ fileName: String) -> URL? {
        guard let documentDirectory = FileManager.documentsDirectory else {return nil}
        return documentDirectory.appendingPathComponent(fileName, isDirectory: false)
    }
}

public extension URL {
    
    var fileContent: Data? {
        return FileManager.default.contents(atPath: path)
    }
}
