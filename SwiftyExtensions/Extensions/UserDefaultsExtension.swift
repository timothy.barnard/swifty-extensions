//
//  UserDefaultsExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy on 08/09/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//
import UIKit

public extension UserDefaults {
    
    /// Stores and checks boolean value if the app has been opened already
    ///
    /// - Returns: Boolean value
    @discardableResult
    static func isFirstLaunch() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
        }
        return isFirstLaunch
    }

    /// Checks if user has been registerd already
    ///
    /// - Returns: Boolean value
    @discardableResult
    static func isRegisteredBefore() -> Bool {
        let hasBeenLaunchedBeforeFlag = "hasRegisterdBefore"
        return UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
    }

    /// Setting value to register user
    static func registered() {
        let hasBeenLaunchedBeforeFlag = "hasRegisterdBefore"
        UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
    }
}
