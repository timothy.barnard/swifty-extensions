//
//  UIButtonExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 24/02/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension UIButton {

    func addShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset =  CGSize(width: 0.0, height: 4.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 5
        self.layer.cornerRadius = 5
        self.layer.shadowOpacity = 0.8
    }
    
    func addRoundedShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset =  CGSize(width: 0.0, height: 4.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = self.frame.width / 2
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.shadowOpacity = 0.8
    }

    func addShadow(_ color: UIColor, offset: CGSize, radius: CGFloat, cornerRadius: CGFloat, opacity: Float) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset =  offset
        self.layer.masksToBounds = false
        self.layer.shadowRadius = radius
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowOpacity = opacity
    }

    func addCornerBorder(borderWidth: CGFloat, cornerRadius: CGFloat, color: UIColor) {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }

    func addBorder(_ borderWidth: CGFloat, color: UIColor) {
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
    }

    func setFab(image: UIImage) {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.8
        layer.cornerRadius = frame.width / 2
        setImage(image, for: .normal)
        contentVerticalAlignment = .fill
        contentHorizontalAlignment = .fill
        imageEdgeInsets = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    func setFabButton(image: UIImage, color: UIColor) {
        backgroundColor = color
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.8
        layer.cornerRadius = frame.width / 2
        setImage(image, for: .normal)
    }
    
    func setFabButton() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.8
        layer.cornerRadius = frame.width / 2
    }
}

public extension UISwitch {

    func addShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset =  CGSize(width: 0.0, height: 2.0)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.6
    }
}
