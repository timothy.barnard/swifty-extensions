//
//  Optional.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import Foundation

public extension Optional where Wrapped == Bool {
    
    /// if value is nil then returns false, else opposite of value
    var not: Bool {
        switch self {
        case .none:
            return false
        case .some(let wrapped):
            return !wrapped
        }
    }
}

public extension Optional { 
    /// Unwrapping optioanl
    ///
    /// - Parameter handler: call back handle to return value
    func then(_ handler: ((Wrapped) -> Void)? = nil) {
        switch self {
        case .none: break
        case .some(let wrapped): handler?(wrapped)
        }
    }
}


public extension Optional where Wrapped == String {
    
    /// check optional string has been set
    var isBlank: Bool {
        return self?.isEmpty ?? true
    }
}
