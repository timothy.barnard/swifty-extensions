//
//  SwiftyExtensions.h
//  SwiftyExtensions
//
//  Created by Timothy on 13/02/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SwiftyExtensions.
FOUNDATION_EXPORT double SwiftyExtensionsVersionNumber;

//! Project version string for SwiftyExtensions.
FOUNDATION_EXPORT const unsigned char SwiftyExtensionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftyExtensions/PublicHeader.h>


